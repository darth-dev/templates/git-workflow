# Git Workflow

![Git Workflow](images/workflow.png)

## Resumo sobre Git workflow

Workflow é a definição, execução e automação de processos de negócios onde tarefas, informações ou documentos são passados de um participante para outro para ação, de acordo com um conjunto de regras procedurais.

Git é um sistema de controle de versão para rastrear mudanças em arquivos de computador e coordenar o trabalho nesses arquivos entre várias pessoas. É usado principalmente para gerenciamento de código-fonte no desenvolvimento de software.

Usando o Git como uma ferramenta poderosa, podemos lidar com o workflow de forma eficiente.

## Vantagens do Git workflow

1. **Simplicidade**: A simplicidade do modelo subjacente do Git torna o workflow fácil de compreender, garantindo máxima produtividade em um período de tempo menor.

2. **Consistência**: O Git fornece um conjunto de comandos consistentes que seguem um padrão sistemático, facilitando a implementação. O padrão semelhante para todas as três categorias de branches - feature, release e hot-fix - minimiza erros no desenvolvimento e nas operações.

3. **Adaptabilidade**: O Git é facilmente adaptável em qualquer estágio do ciclo de vida do projeto, sem impor pré-requisitos. Não importa o quão cedo ou avançado se esteja no desenvolvimento; o git-flow pode ser incluído e adaptado quase instantaneamente.

4. **Clareza**: O Git assegura um estado limpo e decifrável dos branches em qualquer momento do ciclo de vida do projeto.

Usando o Git como uma ferramenta poderosa, podemos lidar com o workflow de forma eficiente, promovendo uma colaboração eficaz e um desenvolvimento de software mais organizado e produtivo.

---------

![Feature branching Workflow](images/bran.png)

O *Feature Branch Workflow* assume um repositório central, e `master` representa o histórico oficial do projeto. Em vez de fazer commit diretamente em seu branch local `master`, os desenvolvedores criam um novo branch toda vez que começam a trabalhar em uma nova funcionalidade. As branches de funcionalidades devem ser criados a partir do branch `develop` que contém o histórico de integração completo do projeto. Quando uma funcionalidade é finalizada, ela é mesclada de volta ao branch `develop`. Isto assegura que o histórico oficial do projeto sempre contenha apenas código de produção final.

As branches de funcionalidades devem ter nomes descritivos, como `pessoas-tabela-ingestao` or `issue-$1061`.

## Como funciona 

1. **Comece com a branch master**

    Todas branches de funcionalidades são criadas a partir do mais recente estado do código no projeto. Esse guia assume que isso é mantido e atualizado na branch `master`.

    ```bash
    git checkout master
    git fetch origin
    git reset --hard origin/master
    ```   
    Isso alterna o `repo` para o branch `master`, puxa os commits mais recentes e redefine a cópia local do `repo` do `master` para corresponder à versão mais recente.

2. **Crie uma branch nova**

    Use uma branch separado para cada funcionalidade ou `issue` em que você trabalha. Após criar uma branch, faça `checkout` dela localmente para que quaisquer alterações que você fizer estejam nessa branch.
    ```bash
    git checkout -b new-feature
    ```   
    Isso faz checkout de um branch chamado new-feature baseado no master, e a flag -b informa ao Git para criar o branch se ele ainda não existir.

3. **Update, add, commit, and push changes**

    Nessa branch, edite, faça o stage e o commit das mudanças da maneira usual, desenvolvendo a funcionalidade com quantos commits forem necessários. Trabalhe na funcionalidade e faça commits como faria normalmente ao usar o Git. Quando estiver pronto, envie seus commits, atualizando o branch de funcionalidade no GitLab.

    ```bash
    git status
    git add <some-file>
    git commit -m "<message>"
    ```

4. **Enviar branch de funcionalidade para o repositório remoto**

    É uma boa ideia enviar a branch de funcionalidade para o repositório central. Isso serve como um backup conveniente e, ao colaborar com outros desenvolvedores, permite que eles tenham acesso para visualizar os commits na nova branch.

    ```bash
    git push -u origin new-feature
    ```
    Este comando envia `nova-funcionalidade` para o repositório central (`origin`), e a flag `-u` o adiciona como uma branch de rastreamento remoto. Após configurar o branch de rastreamento, `git push` pode ser invocado sem nenhum parâmetro para enviar automaticamente o branch `nova-funcionalidade` para o repositório central. Para obter feedback sobre a nova branch de funcionalidade, crie um `pull request` em uma solução de gerenciamento de repositórios como GitLab. A partir daí, você pode adicionar revisores e garantir que tudo esteja pronto antes de fazer o `merge`.

5. **Resolve feedback**

    Agora os colegas de equipe comentam e aprovam os commits enviados. Resolva os comentários localmente, faça um commit e envie as mudanças sugeridas para o `GitLab`. Suas atualizações aparecem no pull request.

6. **Merge seu pull request**

    Antes de fazer o merge, você pode ter que resolver conflitos de `merge` se outras pessoas tiverem feito mudanças no repositório. Quando seu `pull request` for aprovado e estiver livre de conflitos, você poderá adicionar seu código à branch master. Faça o merge a partir do pull request no GitLab.

#### 2.3 Pull requests

Além de isolar o desenvolvimento de funcionalidades, branches possibilitam a discussão de mudanças através de pull requests. Uma vez que alguém completa uma funcionalidade, não a mescla imediatamente na master. Em vez disso, envia a branch da funcionalidade para o servidor central e cria um pull request pedindo para mesclar suas adições na master. Isso dá a outros desenvolvedores a oportunidade de revisar as mudanças antes que elas se tornem parte do código principal.

A revisão de código é um grande benefício dos pull requests, mas eles são projetados para serem uma forma genérica de discutir código. Você pode pensar nos pull requests como uma discussão dedicada a uma branch específica. Isso significa que eles também podem ser usados muito antes no processo de desenvolvimento. Por exemplo, se um desenvolvedor precisa de ajuda com uma funcionalidade particular, tudo o que ele precisa fazer é criar um pull request. As partes interessadas serão notificadas automaticamente e poderão ver a questão ao lado dos commits relevantes.

Uma vez que um pull request é aceito, você precisa garantir que sua master local esteja sincronizada com a master upstream. Em seguida, mescla a branch da funcionalidade na master e envia a master atualizada de volta para o repositório central.

---------

### 3. **Fluxo de Trabalho Gitflow**

#### 3.1 Visão Geral

O Gitflow é realmente apenas uma ideia abstrata de um fluxo de trabalho Git. Isso significa que ele dita quais tipos de branches configurar e como mesclá-los. Vamos abordar os propósitos dos branches abaixo.

#### 3.2 Como funciona

##### 3.2.1 Branches Develop e Master

![Branches Develop e Master](images/git_develop.png)

Em vez de um único branch `master`, esse fluxo de trabalho usa dois branches para registrar o histórico do projeto. O branch `master` armazena o histórico oficial de lançamentos, e o branch `develop` serve como um branch de integração para recursos.

O primeiro passo é complementar o `master` padrão com um branch `develop`. Uma maneira simples de fazer isso é um desenvolvedor criar um branch `develop` vazio localmente e empurrá-lo para o servidor:

```bash
git branch develop
git push -u origin develop
```

Este branch conterá o histórico completo do projeto, enquanto `master` conterá uma versão abreviada. Outros desenvolvedores devem agora clonar o repositório central e criar um branch de rastreamento para `develop`.

![Branches de Funcionalidade](images/git_feature.png)

Cada novo recurso deve residir em seu próprio branch, que pode ser empurrado para o repositório central para backup/colaboração. Mas, em vez de se ramificar a partir do `master`, os branches de `feature` usam `develop` como seu branch pai. Quando um recurso é concluído, ele é mesclado de volta em `develop`. Recursos nunca devem interagir diretamente com `master`.

Note que os branches de `feature`, combinados com o branch `develop`, são, para todos os efeitos, o Fluxo de Trabalho de Branch de Funcionalidade. Mas, o Fluxo de Trabalho Gitflow não para por aí.

Os branches de `feature` são geralmente criados a partir do branch `develop` mais recente.

Os branches de `Feature` geralmente são criados a partir do branch `develop` mais recente.

1. **Creating a feature branch**

    ```bash
    git checkout develop
    git checkout -b feature_branch
    ```

2. **Finishing a feature branch**

    When you’re done with the development work on the `feature`, the next step is to merge the `feature_branch` into `develop`.

    ```bash
    git checkout develop
    git merge feature_branch
    ```

##### 3.2.3 Release Branches

![Release Branches](images/git_release.png)

Once `develop` has acquired enough features for a release (or a predetermined release date is approaching), you fork a `release` branch off of `develop`. Creating this branch starts the next release cycle, so no new features can be added after this point—only bug fixes, documentation generation, and other release-oriented tasks should go in this branch. Once it's ready to ship, the `release` branch gets merged into `master` and tagged with a version number. In addition, it should be merged back into `develop`, which may have progressed since the `release` was initiated.

Using a dedicated branch to prepare releases makes it possible for one team to polish the current release while another team continues working on features for the next release. It also creates well-defined phases of development (e.g., it's easy to say, “This week we're preparing for version 4.0,” and to actually see it in the structure of the repository).

Making `release` branches is another straightforward branching operation. Like `feature` branches, `release` branches are based on the `develop` branch. A new `release` branch can be created using the following methods.

Without the git-flow extensions:

```bash
git checkout develop
git checkout -b release/0.1.0
```

Once the release is ready to ship, it will get merged it into `master` and `develop`, then the release branch will be deleted. It’s important to merge back into `develop` because critical updates may have been added to the release branch and they need to be accessible to new features. If your organization stresses code review, this would be an ideal place for a pull request.

To finish a release branch, use the following methods:

Without the git-flow extensions:

```bash
git checkout develop
git merge release/0.1.0
```

##### 3.2.4 **Hotfix Branches**

![Hotfix Branches](images/git_hotfix.png)

Maintenance or “`hotfix`” branches are used to quickly patch production releases. `Hotfix` branches are a lot like `release` branches and `feature` branches except they're based on `master` instead of `develop`. This is the only branch that should fork directly off of `master`. As soon as the fix is complete, it should be merged into both `master` and `develop` (or the current `release` branch), and `master` should be tagged with an updated version number.

Having a dedicated line of development for bug fixes lets your team address issues without interrupting the rest of the workflow or waiting for the next release cycle. You can think of maintenance branches as ad hoc `release` branches that work directly with `master`. A `hotfix` branch can be created using the following methods:

Without the git-flow extensions:

```bash
git checkout master
git checkout -b hotfix_branch
```

Similar to finishing a `release` branch, a `hotfix` branch gets merged into both master and develop.

```bash
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
$ git flow hotfix finish hotfix_branch
```

#### 3.3 Summary

Some key takeaways to know about Gitflow are:

- The workflow is great for a release-based software workflow.
- Gitflow offers a dedicated channel for hotfixes to production.
 
The overall flow of Gitflow is:

1. A `develop` branch is created from `master`
2. A `release` branch is created from `develop`
3. `Feature` branches are created from `develop`
4. When a `feature` is complete it is merged into the `release` branch
5. When the release branch is done it is merged into develop and `master`
6. If an issue in `master` is detected a `hotfix` branch is created from `master`
7. Once the `hotfix` is complete it is merged to both `develop` and `master`

------------
